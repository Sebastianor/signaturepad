angular.module('starter').controller('MainController', function ($scope, $state, $ionicPopup) {
  

var DrawAll = function()
  {
        for(i = 0; i < array.length; i ++)
    {
      if(array[i].type == "V")
      { 
        ctx.drawImage(check, array[i].X - 20, array[i].Y - 20);
        ctx.stroke();
      }
      else if(array[i].type == "X")
      {
      ctx.drawImage(x, array[i].X-20, array[i].Y-20);
      ctx.stroke();
      }
       else if(array[i].type == "text")
      {
       if(array[i].value.length < 20)
                  {
                    ctx.font="30px Courier";
                    
                  }
                  else if(array[i].value.length >=20)
                  {
                    ctx.font="20px Courier";
                  }
      ctx.fillStyle = "black";
      ctx.fillText(array[i].value, array[i].X, array[i].Y);
      }
       else if(array[i].type == "textarea")
      {
        ctx.font = "20px Courier";
        var zmienna = array[i].value;
        var result="";
        var line = 1;
        for(j=0; j<array[i].value.length;j++)
        {
          result += zmienna[j];
          ctx.fillStyle = "black";
          ctx.fillText(zmienna[j], array[i].X+(result.length-1)*12, array[i].Y+(line-1)*52);
          if(result.length>=65 && result.length<75 && zmienna[j] == " ")
          {
            result = "";
            line +=1;
            if(line == 5)
            {
              break;
            }
          }
           else if(result.length == 75)
          {
            if(line<4)
            {
              ctx.fillStyle = "black";
              ctx.fillText("-", array[i].X+(result.length)*12, array[i].Y+(line-1)*52);
              result="";
              line+=1;
            }
            else
            {
              break;
            }
          }
        }
      }
    }
  }
  var array = [];
  var check = new Image();
  var x = new Image();
  check.src = "img/check.svg";
  x.src ="img/x.svg";
  var canvas = document.getElementById('signatureCanvas');
  canvas.width  = 1024;
  canvas.height = 600;
  var signaturePad = new SignaturePad(canvas);
  $scope.color = 1;
  signaturePad.off();
  //Imię i Nazwisko
  array[0] = {type: "empty", X: 23, Y: 455, X_Last: 546, value: "", title: "Imię i Nazwisko"};
  //Dane Kontaktowe
  array[1] = {type: "empty", X: 582, Y: 455, X_Last: 930, value: "", title: "Dane Kontaktowe"};
  //Model Samochodu
  array[2] = {type: "empty", X: 973, Y: 455, X_Last: 1511, value: "", title: "Model Samochodu"};
  //Numer Karty pracy
  array[3] = {type: "empty", X: 1555, Y: 455, X_Last: 1921, value: "", title: "Numer Karty Pracy"};
  //Numer rejestracyjny
  array[4] = {type: "empty", X: 23, Y: 588, X_Last: 547, value: "", title: "Numer rejestracyjny"};
  //Przebieg w km
  array[5] = {type: "empty", X: 582, Y: 588, X_Last: 928, value: "", title: "Przebieg w km"};
  //NUMER VIN
  array[6] = {type: "empty", X: 973, Y: 588, X_Last: 1512, value: "", title: "Numer identyfikacyjny VIN"};
  //Data
  array[7] = {type: "empty", X: 1555, Y: 588, X_Last: 1922, value: "", title: "Data"};
  //BADANIE SPALIN DZIEŃ MIESIĄC ROK
  array[8] = {type: "empty", X: 275, Y: 1795, X_Last: 320, value: "", title: "Dzień"};
  array[9] = {type: "empty", X: 449, Y: 1795, X_Last: 495, value: "", title: "Miesiąc"};
  array[10] = {type: "empty", X: 575, Y: 1795, X_Last: 619, value: "", title: "Rok"};
  //Doradca serwisowy
  array[11] = {type: "empty", X: 276, Y: 2733, X_Last: 941, value: "", title: "Doradca serwisowy"};
  //Klient
  array[12] = {type: "empty", X: 249, Y: 2820, X_Last: 940, value: "", title: "Klient"};
  //TextArea
  array[13] = {type: "textarea", X: 1004, Y: 2666, X_Last: 1920, value: ""};
  var ctx = canvas.getContext("2d");

  canvas.addEventListener('click', function(event) 
  {
    //DODANIE CHECKA
    if($scope.action == 0)
    { 
      array[array.length] = {type: "V", X: event.offsetX, Y: event.offsetY};
      ctx.drawImage(check, event.offsetX-20, event.offsetY-20 );
      ctx.stroke();
    }
    //POSTAWIENIE KRZYŻYKA
    else if($scope.action == 1)
    {
      array[array.length] = {type: "X", X: event.offsetX, Y: event.offsetY};
      ctx.drawImage(x, event.offsetX -20, event.offsetY-20);
      ctx.stroke();
    }
    //EDYCJA TEKSTU
    else if($scope.action == 3)
    {
      $scope.done = false;
      array.forEach(function(element, index)
      {
        if(element.type == "empty")
        {
          if((event.offsetX >= element.X && event.offsetX <= element.X_Last) && (event.offsetY <= element.Y && event.offsetY >= element.Y -80))
          { 
            $scope.done = true;
            var showPopup = function() 
            {
              $scope.data = {}
              var myPopup = $ionicPopup.show({
              template: '<input type="text" ng-model="data.text">',
              title: element.title,
              subtitle: 'Co chcesz wpisać ?',
              scope: $scope,
              buttons: [
              { text: 'Anuluj' },
              {
              text: '<b>Dodaj</b>',
              type: 'button-positive',
              onTap: function(e) 
              {
                if (!$scope.data.text) 
                {
                  e.preventDefault();
                } 
                else 
                {
                  return $scope.data.text;
                }
              }
              }]});
              myPopup.then(function(res) 
              {
                if(res != undefined)
                {
                  // if(element.title == "Dane Kontaktowe")
                  // {
                  //   console.log("SARARARIRARARARARA");
                  // }
                 if(res.length < 20)
                  {
                    ctx.font="30px Courier";
                    
                  }
                  else if(res.length >=20)
                  {
                    ctx.font="20px Courier";
                  }
                  element.type = "text";
                    element.value = res;
                    ctx.fillStyle = "black";
                    ctx.fillText(res,element.X , element.Y);
                    ctx.stroke();
                }
              });
            };
            showPopup();
          } 
        }
        else if(element.type =="textarea")
        {
          if((event.offsetX <= element.X_Last  && event.offsetX > element.X) && (event.offsetY <= 2872 && event.offsetY  >= element.Y-20))
          {
            $scope.done = true;
            var showPopup = function() 
            {
              $scope.data = {}
              $scope.data.text = element.value;
              var myPopup = $ionicPopup.show({
              template: '<input type="text" ng-model="data.text">',
              title: 'Dodatkowe uwagi',
              scope: $scope,
              buttons: [
              { text: 'Anuluj' },
              {
              text: '<b>Dodaj</b>',
              type: 'button-positive',
              onTap: function(e) 
              {
                if (!$scope.data.text) 
                {
                  //don't allow the user to close unless he enters wifi password
                  e.preventDefault();
                } 
                else 
                {
                  return $scope.data.text;
                }
              }
            }]
          });
  myPopup.then(function(res) {
    if(res != undefined)
    {
      element.value = res;
      canvas.width = canvas.width;
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.save();
      ctx.stroke();
      DrawAll();
    }
  });
 };
 showPopup();
      }

    }
    else if(element.type == "text")
    {  $scope.rozmiar ="";
      
      if(element.value.length >= 20)
      {
        $scope.rozmiar = 12;
      }
      else
      {
        $scope.rozmiar = 18.1;
      }
       if((event.offsetX <= element.X + element.value.length * $scope.rozmiar && event.offsetX > element.X) && (event.offsetY <= element.Y && event.offsetY  >= element.Y - 20))
            {
          $scope.done = true;
          var showPopup = function() {
          $scope.data = {}
          $scope.data.text = element.value;
          var myPopup = $ionicPopup.show({
          template: '<input type="text" ng-model="data.text">',
          title: 'Podaj ponownie poprawioną wartość ?',
          scope: $scope,
          buttons: [
          { text: 'Anuluj' },
          {
          text: '<b>Zmień</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.data.text) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
            return $scope.data.text;
          }
        }
      }
    ]
  });
  myPopup.then(function(res) {
    if(res != undefined)
    {
      element.type = "text";
      element.value = res;
       canvas.width = canvas.width;
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.save();
      ctx.stroke();
      DrawAll();
    }
  });
 };
 showPopup();
            }
    } 
     });
  if($scope.done != true)
  {
     var showPopup = function() {
  $scope.data = {}
  var myPopup = $ionicPopup.show({
    template: '<input type="text" ng-model="data.text">',
    title: 'Co chcesz wpisać ?',
    scope: $scope,
    buttons: [
      { text: 'Anuluj' },
      {
        text: '<b>Dodaj</b>',
        type: 'button-positive',
        onTap: function(e) {
          if (!$scope.data.text) {
            e.preventDefault();
          } else {
            return $scope.data.text;
          }
        }
      }
    ]
  });
  myPopup.then(function(res) {
    if(res == undefined)
    {

    }
    else
    {
      ctx.font = "30px Courier";
      ctx.fillStyle = "black";
      ctx.fillText(res ,event.offsetX, event.offsetY);
      array[array.length] = {type: "text", value: res, X: event.offsetX, Y: event.offsetY};
   }
  });
 };
 showPopup();
  }


  }
  //CZYSZCZENIE DANEGO ELEMENTU
  else if($scope.action == 4)
  {
    for(i = 0; i < array.length; i ++)
     { 
      if(array[i].type == "text")
        { 
          $scope.rozmiar = "";
    if(array[i].value.length >= 30)
      {
        $scope.rozmiar = 12;
      }
      else
      {
        $scope.rozmiar = 18.1;
      }  
          if((event.offsetX <= array[i].X + array[i].value.length * $scope.rozmiar && event.offsetX > array[i].X) && (event.offsetY <= array[i].Y && event.offsetY  >= array[i].Y - 40))
            {
              
             if(i == 0 || i == 1 ||i == 2 ||i == 3 ||i == 4 ||i == 5 ||i == 6 ||i == 7 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12)
             {
                array[i].value = "";
                array[i].type  = "empty";
             }
             else
              {
                array.splice(i, 1);
              }
            }

        }
        else if(array[i].type =="textarea")
        {
          if((event.offsetX <= array[i].X_Last  && event.offsetX > array[i].X) && (event.offsetY <= 2829 && event.offsetY  >= array[i].Y-20))
          {
            array[i].value = "";
          }
        }
      else
      {

          if((event.offsetX <= array[i].X + 25 && event.offsetX >= array[i].X - 25) && (event.offsetY <= array[i].Y + 25 && event.offsetY >= array[i].Y - 25))
          {
          array.splice(i, 1);
          }
      }
    }
    canvas.width = canvas.width;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.save();
    ctx.stroke();
    DrawAll();
  }
  else if($scope.action == 5)
  {
    console.log("X:" + event.offsetX);
     console.log("Y:" + event.offsetY);
  }
});

  

    $scope.setColor = function(color)
    {
      if(color == 1)
      {
        signaturePad.penColor = "black";
      }
      else if(color == 2)
      {
        signaturePad.penColor = "red";
        console.log('tutaj');
      }
      else if(color == 3)
      {
        signaturePad.penColor = "yellow";
      }
       else if(color == 4)
      {
        signaturePad.penColor = "green";
      }
      else if(color == 5)
      {
        signaturePad.penColor = "blue";
      }
      $scope.color = color;
    } 

    $scope.choose = function(action)
    {
      
     $scope.action = action;
     if(action == 5)
     {
      signaturePad.on();
     }
     else
     {
      signaturePad.off();
     }
    }
    $scope.SwipeLeft = function()
    {
      $scope.left = true;
    }
    $scope.SwipeRight = function()
    {

      $scope.left = false;
    }
  
    $scope.save = function()
    {
      $scope.prev = signaturePad.toDataURL();
      signaturePad.clear();
    }
  $scope.reload = function()
  {
    signaturePad.fromDataURL($scope.prev);
  }


 });