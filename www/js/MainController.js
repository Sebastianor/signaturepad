angular.module('starter').controller('MainController', function ($scope, $state, $ionicPopup, $ionicScrollDelegate) {
  



  var canvas = document.getElementById('signatureCanvas');
  canvas.width  = 3000;
  canvas.height = 3000;
  var signaturePad = new SignaturePad(canvas);
  $scope.color = 1;
  $scope.zoom = 1;
  signaturePad.off();


    $scope.setColor = function(color)
    {
      if(color == 1)
      {
        signaturePad.penColor = "black";
      }
      else if(color == 2)
      {
        signaturePad.penColor = "red";
        console.log('tutaj');
      }
      else if(color == 3)
      {
        signaturePad.penColor = "yellow";
      }
       else if(color == 4)
      {
        signaturePad.penColor = "green";
      }
      else if(color == 5)
      {
        signaturePad.penColor = "blue";
      }
      $scope.color = color;
    } 

    $scope.choose = function(action)
    {
      console.log('that works fine');
     $scope.action = action;
     if(action == 5)
     {
      signaturePad.on();
     }
     else
     {
      signaturePad.off();
     }
    }
    signaturePad.onBegin = function()
    {
      $ionicScrollDelegate.$getByHandle('canvas').freezeScroll(true);
    
    }
    signaturePad.onEnd = function()
    {
      $ionicScrollDelegate.$getByHandle('canvas').freezeScroll(false);
    }

   $scope.SetZoom = function(zoom)
   {
      $ionicScrollDelegate.$getByHandle('canvas').zoomTo(zoom);
   }


 });